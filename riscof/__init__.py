# See LICENSE.incore for details

"""Top-level package for RISCOF - RISC-V Compliance Framework."""

__author__ = """InCore Semiconductors Pvt Ltd"""
__email__ = 'info@incoresemi.com'
__version__ = '1.18.1'
